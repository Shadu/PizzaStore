using Microsoft.EntityFrameworkCore;
using PizzaStore.DBConnection;
using PizzaStore.Models;

namespace PizzaEndPoint
{
    // Instanced version class
    //
    // public class PizzaEndPointConfig
    // {
    //     public PizzaEndPointConfig(WebApplication app)
    //     {
    //         app.MapGet("/pizzas", async (PizzaDb db) => await db.Pizzas.ToListAsync());
    //         app.MapPost("/pizza", async (PizzaDb db, Pizza pizza) =>
    //         {
    //             await db.Pizzas.AddAsync(pizza);
    //             await db.SaveChangesAsync();
    //             return Results.Created($"/pizza/{pizza.Id}", pizza);
    //         });

    //         app.MapGet("/pizza/{id}", async (PizzaDb db, int id) => await db.Pizzas.FindAsync(id));

    //         app.MapPut("/pizza/{id}", async (PizzaDb db, Pizza updatePizza, int id) =>
    //         {
    //             var pizza = await db.Pizzas.FindAsync(id);
    //             if (pizza is null) return Results.NotFound();
    //             pizza.Name = updatePizza.Name;
    //             pizza.Description = updatePizza.Description;
    //             await db.SaveChangesAsync();
    //             return Results.NoContent();
    //         });

    //         group.MapDelete("/pizza/{id}", async (PizzaDb db, int id) =>
    //         {
    //             var pizza = await db.Pizzas.FindAsync(id);
    //             if (pizza is null)
    //             {
    //                 return Results.NotFound();
    //             }
    //             db.Pizzas.Remove(pizza);
    //             await db.SaveChangesAsync();
    //             return Results.Ok();
    //         });
    //     }

    //     ~PizzaEndPointConfig()
    //     {
    //     }
    // }


    // Static version class

    public static class PizzaStaticEndPointConfig
    {

        // Method for creating the needed endpoints.
        // Allows for defining Authorization for a group instead of per endpoint.
        public static void RouteGroup(WebApplication app)
        {
            app.MapGroup("/pizza")
                .PizzaEndpoints()
                .RequireAuthorization("admin_greetings");

            app.MapGet("/pizzas", async (PizzaDb db) => await db.Pizzas.FromSqlRaw<Pizza>("EXEC dbo.PizzasList").ToListAsync());
        }

        // Method for supplying the group of endpoints needed by MapGroup.
        public static RouteGroupBuilder PizzaEndpoints(this RouteGroupBuilder group)
        {
            group.MapPost("/", async (PizzaDb db, Pizza pizza) =>
            {
                await db.Pizzas.AddAsync(pizza);
                await db.SaveChangesAsync();
                return Results.Created($"/pizza/{pizza.Id}", pizza);
            });

            group.MapGet("/{id}", async (PizzaDb db, int id) => await db.Pizzas.FindAsync(id));

            group.MapPut("/{id}", async (PizzaDb db, Pizza updatePizza, int id) =>
            {
                var pizza = await db.Pizzas.FindAsync(id);
                if (pizza is null) return Results.NotFound();
                pizza.Name = updatePizza.Name;
                pizza.Description = updatePizza.Description;
                await db.SaveChangesAsync();
                return Results.NoContent();
            });

            group.MapDelete("/{id}", async (PizzaDb db, int id) =>
            {
                var pizza = await db.Pizzas.FindAsync(id);
                if (pizza is null)
                {
                    return Results.NotFound();
                }
                db.Pizzas.Remove(pizza);
                await db.SaveChangesAsync();
                return Results.Ok();
            });

            return group;
        }

        // Method with all the endpoints seperate, but requires defining the Authorization per endpoint.
        public static void EndPoint(WebApplication app)
        {
            // app.MapGet("/pizzas", async (PizzaDb db) => await db.Pizzas.ToListAsync());

            // Using storedprocedure call
            app.MapGet("/pizzas", async (PizzaDb db) => await db.Pizzas.FromSqlRaw<Pizza>("EXEC dbo.PizzasList").ToListAsync());

            app.MapPost("/pizza", async (PizzaDb db, Pizza pizza) =>
            {
                await db.Pizzas.AddAsync(pizza);
                await db.SaveChangesAsync();
                return Results.Created($"/pizza/{pizza.Id}", pizza);
            }).RequireAuthorization("admin_greetings");

            app.MapGet("/pizza/{id}", async (PizzaDb db, int id) => await db.Pizzas.FindAsync(id));

            app.MapPut("/pizza/{id}", async (PizzaDb db, Pizza updatePizza, int id) =>
            {
                var pizza = await db.Pizzas.FindAsync(id);
                if (pizza is null) return Results.NotFound();
                pizza.Name = updatePizza.Name;
                pizza.Description = updatePizza.Description;
                await db.SaveChangesAsync();
                return Results.NoContent();
            }).RequireAuthorization("admin_greetings");

            app.MapDelete("/pizza/{id}", async (PizzaDb db, int id) =>
            {
                var pizza = await db.Pizzas.FindAsync(id);
                if (pizza is null)
                {
                    return Results.NotFound();
                }
                db.Pizzas.Remove(pizza);
                await db.SaveChangesAsync();
                return Results.Ok();
            }).RequireAuthorization("admin_greetings");
        }
    }
}
