using Microsoft.OpenApi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using PizzaStore.DBConnection;

using PizzaEndPoint;
using CreateToken;

namespace Program
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Grabbing the connectionstring from the appsettings.json file.
            var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");

            builder.Services.AddEndpointsApiExplorer();

            // Enabling authentication on the server and setting the validation parameters.
            builder.Services.AddAuthentication().AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration.GetValue<string>("AppSettings:SecretKey"))),
                    ValidAudiences = new[] { "http://localhost:53853", "https://localhost:44350", "http://localhost:5046", "https://localhost:7182" },
                    ValidIssuer = "dotnet-user-jwts"

                };
            });

            // Adding the policy to check for.
            builder.Services.AddAuthorizationBuilder().AddPolicy("admin_greetings", policy => policy
            .RequireRole("admin")
            .RequireClaim("scope", "greetings_api"));

            // Setting the connection to the sql server.
            builder.Services.AddDbContext<PizzaDb>(options => options.UseSqlServer(connectionString));
            // builder.Services.AddScoped<IConfiguration, >

            // Adding swagger and telling it to use authorization.
            builder.Services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("V1", new OpenApiInfo { Title = "PizzaStore API", Description = "Making the Pizzas you love", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Id = "Bearer",
                                    Type = ReferenceType.SecurityScheme
                                }
                            },
                            new string[] {}
                        }
                });
            });

            var app = builder.Build();



            // Url and port to run the app on.
            // app.Urls.Add("http://localhost:5046");

            // Enabling swagger.
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/V1/swagger.json", "PizzaStore API V1");
            });

            // Route for index.
            app.MapGet("/", () => "Hello World!");

            // Route for getting a token supplied for the api.
            app.MapGet("/api/login", () => CreatingToken.CreateAccessToken());

            // Instanced version class call.
            // PizzaEndPointConfig pizza = new PizzaEndPointConfig(app);

            // Static version class call.
            // PizzaStaticEndPointConfig.EndPoint(app);
            PizzaStaticEndPointConfig.RouteGroup(app);

            app.Run();
        }
    }
}
