using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace CreateToken
{
    public static class CreatingToken
    {

        private static readonly IConfiguration _configuration;

        // Creates a token and returns it to the call.
        public static String CreateAccessToken()
        {

            // String K = "12345678901234567890123456789012345678901234567890123456789012345678901234567890";
            // var key = Encoding.UTF8.GetBytes(K);

            // Grabbing the key from the appsettings.json file
            var secretKey = _configuration.GetValue<String>("AppSettings:SecretKey");

            // SymmetricSecurityKey can't work with a string, UTF8 is chosen here to convert to.
            var key = Encoding.UTF8.GetBytes(secretKey);
            var skey = new SymmetricSecurityKey(key);

            // Creates a signature to supply with the key.
            var SignedCredential = new SigningCredentials(skey, SecurityAlgorithms.HmacSha256Signature);

            // Generates a unique id for the token.
            var jti = Guid.NewGuid().ToString();

            // Defining the claims to be used in the token.
            var uClaims = new ClaimsIdentity(new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName,"shadu"),
                new Claim(JwtRegisteredClaimNames.Sub,"shadu"),
                new Claim(JwtRegisteredClaimNames.Jti,jti),
                new Claim("scope","greetings_api"),
                new Claim("role","admin"),
                new Claim(JwtRegisteredClaimNames.Aud,"http://localhost:53853"),
                new Claim(JwtRegisteredClaimNames.Aud,"https://localhost:44350"),
                new Claim(JwtRegisteredClaimNames.Aud,"http://localhost:5046"),
                new Claim(JwtRegisteredClaimNames.Aud,"https://localhost:7182"),
            });

            // Setting an expiry date for the token.
            var expires = DateTime.UtcNow.AddDays(1);

            // Setting the "body" of the token.
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = uClaims,
                Expires = expires,
                Issuer = "dotnet-user-jwts",
                SigningCredentials = SignedCredential,
            };

            // Creating the token.
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenJwt = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(tokenJwt);

            return token;
        }

    }
}
